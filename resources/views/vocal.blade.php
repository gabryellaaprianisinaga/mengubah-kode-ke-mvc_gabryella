<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="CSS/vocal.css">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">


    <title>Vocal</title>
</head>
<body>
  <div class="container">
      <div class="card text-center m-5 p-5 bg-success">
          <h1 class="fw-bold text-white">HITUNG HURUF VOCAL</h1>
          <br>
          <div>
              <label for="" class="text-white fw-bold">Masukkan kata Random</label>
              <input class="input form-control" type="text">
          </div>
          <br>
          <button class="generate btn btn-primary">Hitung Vokal</button>
          <br><br>
          <span class="text-white fw-bold">Hasil : </span>
          <span class="hasil text-white h1"></span>
      </div>

  </div>
  <script src="JS/vocal.js"></script>
</body>
</html>
