<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">


    <link rel="stylesheet" href="CSS/style.css">

  <title>Kalkulator</title>
</head>
<body>
  <div class="container">
   <div class="card text-center m-5 p-5 bg-success">
       <h1 class="text-white fw-bold">CALCULATOR SEDERHANA</h1>
       <div>
           <label for="" class="h6 fw-bold text-white">Input Angka 1</label>
           <input class="value1" type="number">
       </div>
       <div>
           <label for="" class="h6 fw-bold text-white">Input Angka 2</label>
           <input class="value2" type="number">
       </div>

       <br>

       <div>
           <label for="" class="h6 fw-bold text-white">Operasi : </label>
           <button class="jumlah btn btn-info text-white">jumlah (+)</button>
           <button class="kurang btn btn-info text-white">kurang (-)</button>
           <button class="kali btn btn-info text-white">kali (x)</button>
           <button class="bagi btn btn-info text-white">bagi (/)</button>
       </div>

       <br>

       <div>
           <span class="text-white">Output : </span>
           <span class = "hasil text-white h1"></span>
       </div>
   </div>

  </div>





  <script src="JS/script.js"></script>
</body>
</html>
