"use strict";

let value1 = document.querySelector(".value1");
let value2 = document.querySelector(".value2");
const bagi = document.querySelector(".bagi");
let hasil = document.querySelector(".hasil");
const jumlah = document.querySelector(".jumlah");
const kali = document.querySelector(".kali");
const kurang = document.querySelector(".kurang");


let perhitungan;

jumlah.addEventListener("click", function () {
    perhitungan = parseInt(value1.value) + parseInt(value2.value);
    hasil.textContent = perhitungan;
});

kurang.addEventListener("click", function () {
    perhitungan = parseInt(value1.value) - parseInt(value2.value);
    hasil.textContent = perhitungan;
});

kali.addEventListener("click", function () {
    perhitungan = parseInt(value1.value) * parseInt(value2.value);
    hasil.textContent = perhitungan;
});

bagi.addEventListener("click", function () {
    perhitungan = parseInt(value1.value) / parseInt(value2.value);
    hasil.textContent = perhitungan;
});
